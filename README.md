# Readme
Small script used to brute force access to ssh server during the CTF finals. Asynchroniously loops through multiple private key files. Stops when right private key file is found.