# Import the os module, for the os.walk function
import os
import asyncio
import runner

# Set the directory you want to start from
rootDir = '.'
lst = []
totalcounter = 0

for dirName, subdirList, fileList in os.walk(rootDir):
    print('Found directory: %s' % dirName)
    for fname in fileList:
        if '.pub' in fname:
            totalcounter += 1
            lst.append(asyncio.ensure_future(runner.run('{}/{}'.format(dirName,fname[:-4]))))


runner.total = totalcounter
tests = asyncio.gather(*lst)
asyncio.get_event_loop().run_until_complete(tests)
