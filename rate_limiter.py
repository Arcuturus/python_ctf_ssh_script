import asyncio
import time
from collections import deque


class AsyncRateLimiter:

    def __init__(self, max_calls, period=1):
        self.semaphore = asyncio.BoundedSemaphore(value=max_calls)
        self.calls = deque()
        self.period = period

    async def __release_semaphore(self, delay):
        delayed_time = delay - self.period
        if delayed_time < 0:
            await asyncio.sleep(abs(delayed_time))
        self.semaphore.release()

    async def __aenter__(self):
        await self.semaphore.acquire()
        self.calls.append(time.time())
        return self

    async def __aexit__(self, *args):
        time_diff = time.time() - self.calls.popleft()
        await self.__release_semaphore(time_diff)
